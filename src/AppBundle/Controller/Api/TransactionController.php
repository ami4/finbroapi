<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Transaction;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class TransactionController extends Controller
{
    private $categoryRepository;

    private $userRepository;

    private $transactionRepository;

    private $em;

    /**
     * @Route("/api/v01/transaction/add")
     * @Method("GET")
     */
    public function addAction(Request $request)
    {
        $this->em = $this->get('doctrine')->getManager();
        $this->categoryRepository = $this->get('doctrine')->getManager()->getRepository('AppBundle:Category');

        $user = $this->getUser($request->getClientIp());
        $category = $this->categoryRepository->find($request->query->get('category_id'));

        if (is_null($category)) {
            throw $this->createNotFoundException('Category not found');
        }

        $transaction = new Transaction();
        $transaction->setSumm($request->query->get('sum'));
        $transaction->setDate(new \DateTime($request->query->get('date')));
        $transaction->setCategory($category);
        $transaction->setUser($user);

        $this->em->persist($transaction);
        $this->em->flush();

        return new JsonResponse(['success' => true, 'object' => (string) $transaction]);
    }

    /**
     * @Route("/api/v01/transaction/get")
     * @Method("GET")
     */
    public function getAction(Request $request)
    {
        $this->transactionRepository = $this->get('doctrine')->getManager()->getRepository('AppBundle:Transaction');

        $user = $this->getUser($request->getClientIp());
        $transactions = $this->transactionRepository->findBy(['user' => $user]);
        $objects = [];

        /** @var Transaction $transaction */
        foreach ($transactions as $transaction) {
            $objects[] = [
                'sum' => $transaction->getSumm(),
                'date' => $transaction->getDate()->format('d-m-Y'),
                'user' => $transaction->getUser()->getName(),
                'category' => $transaction->getCategory()->getName(),
            ];
        }

        return new JsonResponse(['success' => true, 'objects' => $objects]);
    }

    /**
     * @param string $ip
     *
     * @return User
     */
    protected function getUser($ip = null)
    {
        $this->userRepository = $this->get('doctrine')->getManager()->getRepository('AppBundle:User');
        $user = $this->userRepository->findOneBy(['ip' => $ip]);

        if (is_null($user)) {
            throw $this->createNotFoundException('User not found');
        }

        return $user;
    }
}
