<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CategoryController extends Controller
{
    private $em;

    /**
     * @Route("/api/v01/category/add")
     * @Method("GET")
     */
    public function addAction(Request $request)
    {
        $this->em = $this->get('doctrine')->getManager();

        $category = new Category();
        $category->setName($request->query->get('name'));

        $this->em->persist($category);
        $this->em->flush();

        return new JsonResponse(['success' => true, 'object' => (string) $category]);
    }
}
