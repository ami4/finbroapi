<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class UserController extends Controller
{
    private $em;

    /**
     * @Route("/api/v01/user/add")
     * @Method("GET")
     */
    public function addAction(Request $request)
    {
        $this->em = $this->get('doctrine')->getManager();

        $user = new User();
        $user->setName($request->query->get('name'));
        $user->setIp($request->query->get('ip') ?? $request->getClientIp());

        $this->em->persist($user);
        $this->em->flush();

        return new JsonResponse(['success' => true, 'object' => (string) $user]);
    }
}
